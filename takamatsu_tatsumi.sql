-- MySQL dump 10.13  Distrib 5.6.11, for Win64 (x86_64)
--
-- Host: localhost    Database: takamatsu_tatsumi
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'本社'),(2,'支店A'),(3,'支店B'),(4,'支店C');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(500) NOT NULL,
  `writter_id` int(11) NOT NULL,
  `is_delete` bit(1) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'test',24,'','2019-09-25 00:49:11',1),(2,'一般社員からの書き込みテスト',22,'','2019-09-25 00:49:11',4),(3,'delete_test',24,'','2019-09-25 00:49:11',2),(4,'delete_test2',24,'','2019-09-25 00:49:11',1),(5,'delete_test6',24,'','2019-09-25 00:49:11',6),(6,'test',24,'','2019-09-25 00:49:11',2),(7,'test8',24,'','2019-09-25 00:49:11',1),(8,'いいいいい',24,'','2019-09-25 00:49:11',3),(9,'select * from users;',24,'','2019-09-25 00:49:11',3),(10,'; select * from users;',24,'','2019-09-25 00:49:11',3),(11,'sa',24,'','2019-09-25 00:49:11',7),(12,'xsasdsa',24,'','2019-09-25 00:49:11',6),(13,'あｓｄｆｇｈｊｋｌ；：￥',24,'','2019-09-24 09:06:50',3),(14,'江頭',24,'','2019-09-24 09:07:06',2),(15,'コメントを打つテスト',26,'\0','2019-09-27 00:29:05',1),(16,'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',26,'','2019-09-27 00:37:14',1),(17,'aaa',24,'\0','2019-09-27 02:47:37',6),(18,'aaa',24,'\0','2019-09-27 02:47:50',1),(19,'wwww',24,'\0','2019-09-27 07:52:34',6),(20,'wwwwwwwwwww',24,'\0','2019-09-27 07:52:40',6);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'総務人事担当者'),(2,'情報管理担当者'),(3,'支店長'),(4,'社員1'),(5,'社員2');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `text` text NOT NULL,
  `category` varchar(10) NOT NULL,
  `writter_id` int(11) NOT NULL,
  `is_delete` bit(1) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'test','test1','test',24,'\0','2019-09-24 08:35:46'),(2,'test2','test2','test',24,'\0','2019-09-24 08:35:46'),(3,'test3','test3','test',24,'\0','2019-09-24 08:35:46'),(4,'test4','test4','test',24,'\0','2019-09-24 08:35:46'),(5,'test5','test5','test5',22,'\0','2019-09-24 08:35:46'),(6,'delete6_test','test','test',24,'\0','2019-09-24 08:35:46'),(7,'test9','test9','test',24,'','2019-09-24 08:54:25'),(8,'test10','test10','test',24,'','2019-09-24 08:54:07'),(9,'test11','test11','テストなのさ！',24,'\0','2019-09-25 01:27:47'),(10,'test12','defsgh','postss',24,'\0','2019-09-25 09:05:31'),(11,'testtesttest','aaaaaaaaaa','aaaaaaaaa',24,'','2019-09-26 00:32:51'),(12,'aaaaaaaaaaaa','aaaaaaaaaaaaa','aaaaaaaaa',24,'','2019-09-26 00:33:01'),(13,'No16のテスト','No16のテスト','No16のテスト',26,'\0','2019-09-27 01:00:08'),(14,'るーるるるる','るるるるるる','るるるるるる',24,'\0','2019-09-27 07:29:09'),(15,'れれれれれ','れれれれれれ','れれれれれえ',24,'\0','2019-09-27 07:29:24');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(10) NOT NULL,
  `branch_code` int(11) NOT NULL,
  `department_code` int(11) NOT NULL,
  `is_stopped` bit(1) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `login_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (16,'hoge1hoge','THFtTPIRx7fS8yM8lBdxrQUH6lus-TtJJ2aqQa6fcg0','hoge1',4,3,'','2019-09-25 02:39:45','2019-09-25 02:39:45'),(17,'hoge2hoge','THFtTPIRx7fS8yM8lBdxrQUH6lus-TtJJ2aqQa6fcg0','hoge2',2,3,'\0','2019-09-25 01:43:00','2019-09-25 01:43:00'),(18,'hoge05','47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU','hoge5',1,4,'\0','2019-09-25 01:01:05','0000-00-00 00:00:00'),(19,'hogehoge','47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU','hoge hoge',3,4,'\0','2019-09-24 08:09:37','2019-09-20 00:03:21'),(20,'tekeritekeri','47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU','テケリ',3,4,'\0','2019-09-24 08:09:39','0000-00-00 00:00:00'),(22,'employee1','THFtTPIRx7fS8yM8lBdxrQUH6lus-TtJJ2aqQa6fcg0','社員その1',1,1,'\0','2019-09-24 08:09:40','2019-09-24 00:12:31'),(23,'tekeri2','THFtTPIRx7fS8yM8lBdxrQUH6lus-TtJJ2aqQa6fcg0','テケリリリ',1,2,'\0','2019-09-18 00:52:04','0000-00-00 00:00:00'),(24,'adminall','gW4R1Sx9IBPqCRZ0XnoHakvSUHxeN_Y8zFaVAmwTRhA','admin',1,1,'\0','2019-09-27 07:59:55','2019-09-27 07:59:55'),(25,'adminall23','47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU','admin2',1,2,'','2019-09-20 07:23:12','0000-00-00 00:00:00'),(26,'employee2','THFtTPIRx7fS8yM8lBdxrQUH6lus-TtJJ2aqQa6fcg0','社員その2',2,4,'\0','2019-09-27 00:13:41','2019-09-27 00:13:41'),(29,'employee03','THFtTPIRx7fS8yM8lBdxrQUH6lus-TtJJ2aqQa6fcg0','社員その03 一般',4,5,'\0','2019-09-24 06:28:57','0000-00-00 00:00:00'),(30,'employee04','THFtTPIRx7fS8yM8lBdxrQUH6lus-TtJJ2aqQa6fcg0','停止された社員',3,4,'\0','2019-09-26 08:36:18','2019-09-26 08:36:18'),(31,'employee05','THFtTPIRx7fS8yM8lBdxrQUH6lus-TtJJ2aqQa6fcg0','一般社員その2',2,3,'\0','2019-09-26 00:08:39','0000-00-00 00:00:00'),(32,'employee06','THFtTPIRx7fS8yM8lBdxrQUH6lus-TtJJ2aqQa6fcg0','一般社員その3',3,4,'\0','2019-09-26 00:15:13','0000-00-00 00:00:00'),(33,'infomession1','03DIRg0J2DYCpgFpXcLUFa9jTXXbedg2YrZU7nrRfFc','情報員その1',1,2,'','2019-09-26 00:26:23','2019-09-26 00:26:23'),(34,'testuser01','m9Qk9z03_mioo8_zIi-lE8VHtL5A2LSz7vC9TzX7egE','テストユーザー',1,1,'\0','2019-09-27 01:54:38','0000-00-00 00:00:00'),(35,'ohohoi','oQDXrl4mvqPgqeAUf4DMbmi8GED33FCSe86YDeMifJk','ohohi',1,2,'','2019-09-27 02:14:49','0000-00-00 00:00:00'),(36,'hoge2hoge2','oQDXrl4mvqPgqeAUf4DMbmi8GED33FCSe86YDeMifJk','ohohoi2',1,4,'','2019-09-27 02:17:01','0000-00-00 00:00:00'),(37,'testuser03','m9Qk9z03_mioo8_zIi-lE8VHtL5A2LSz7vC9TzX7egE','テストユーザー03',1,2,'\0','2019-09-27 02:49:53','0000-00-00 00:00:00'),(38,'testuser04','m9Qk9z03_mioo8_zIi-lE8VHtL5A2LSz7vC9TzX7egE','テストユーザー04',1,2,'\0','2019-09-27 02:57:11','0000-00-00 00:00:00'),(39,'testuser05','m9Qk9z03_mioo8_zIi-lE8VHtL5A2LSz7vC9TzX7egE','テストユーザー05',2,3,'\0','2019-09-27 04:08:54','0000-00-00 00:00:00'),(40,'testuser06','m9Qk9z03_mioo8_zIi-lE8VHtL5A2LSz7vC9TzX7egE','テストユーザー06',3,3,'\0','2019-09-27 04:12:03','0000-00-00 00:00:00'),(41,'testuser07','m9Qk9z03_mioo8_zIi-lE8VHtL5A2LSz7vC9TzX7egE','テストユーザー07',4,3,'\0','2019-09-27 04:16:21','0000-00-00 00:00:00'),(42,'testuser08','m9Qk9z03_mioo8_zIi-lE8VHtL5A2LSz7vC9TzX7egE','テストユーザー08',2,4,'\0','2019-09-27 04:17:57','0000-00-00 00:00:00'),(43,'testuser09','m9Qk9z03_mioo8_zIi-lE8VHtL5A2LSz7vC9TzX7egE','テストユーザー09',3,4,'\0','2019-09-27 04:20:30','0000-00-00 00:00:00'),(44,'testuser10','m9Qk9z03_mioo8_zIi-lE8VHtL5A2LSz7vC9TzX7egE','テストユーザー10',4,4,'\0','2019-09-27 04:22:31','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-30  9:27:00
