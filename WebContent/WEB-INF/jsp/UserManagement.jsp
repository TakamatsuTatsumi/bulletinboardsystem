<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理画面</title>
</head>
<body>
<script>
	function alertFunction1(message, id) {
		if(confirm("ユーザーを" + message + "してもいいですか？")){
			var str = "stopped?stoppedId=" + id;
			window.location.href = str;
		} else {
			alert( "データの送信をキャンセルします。") ;
		}
	}
</script>
<div class="main-content">
	<a href="signup">ユーザー新規作成</a>　　　<a href="home">戻る</a>
	<div class="title">ユーザー一覧</div>
	<div class="userList">
			<table>
				<tr><th>id</th><th>名前</th><th>ログインID</th><th>所属</th><th>役職・部署</th><th>登録日</th><th>アカウント状態</th></tr>
				<c:forEach items="${userList}" var="editUser">
					<tr>
						<td><a href="<c:url value="settings"><c:param name="id" value="${ editUser.id }" /></c:url>" >${ editUser.id }</a></td>
						<td>${ editUser.name }</td>
						<td>${ editUser.loginId }</td>
						<td>${ editUser.branchCode }</td>
						<td>${ editUser.departmentCode }</td>
						<td>${ editUser.createdDate }</td>
						<td><button onclick="alertFunction1('${editUser.stopped ? '稼働' : '停止'}', '${editUser.id}')" >${ editUser.stopped ? "停止" : "稼働" }</button></td>
					</tr>
				</c:forEach>
			</table>
	</div>
	<a href="signup">ユーザー新規作成</a>　　　<a href="home">戻る</a>
</div>
</body>
</html>