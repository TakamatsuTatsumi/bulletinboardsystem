<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <div class="inputfrom">
	            <form action="signup" method="post">
	                <br />
	                <label for="name">名前</label> <input name="name" id="name" value="${newUser.name}" />（名前はあなたの公開プロフィールに表示されます）<br />
	                <label for="loginId">ログインID</label> <input name="loginId"id="loginId" value="${newUser.loginId}" /> <br />
	                <label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />
	                <label for="repassword">確認用パスワード</label> <input name="repassword" type="password" id="repassword" /> <br />
	                <label for="branchCode">支店</label> <select name="branchCode" id="branchCode">
	                	<option value="">選択してください</option>
	                	<c:forEach items="${branchList}" var="branch">
	                		<c:set var="branchName" value="${branch.name}" />
	                		<option value="${ branch.name }" ${ not empty newUser.branchCode && newUser.branchCode.equals(branchName) ? " selected=\"selected\"" : ""}>${ branch.name }</option>
	                	</c:forEach>
	                </select><br />
	                <label for="departmentCode">部署・役職</label> <select name="departmentCode" id="departmentCode">
	                	<option value="">選択してください</option>
	                	<c:forEach items="${departmentList}" var="department">
	                		<c:set var="departmentName" value="${department.name}" />
	                		<option value="${ department.name }" ${ not empty newUser.branchCode != null && newUser.departmentCode.equals(departmentName) ? " selected=\"selected\"" : ""}>${ department.name }</option>
	                	</c:forEach>
	                </select><br />
	                <br /><div class = "submits"><button type="submit">登録</button>　　　　　<a href="UserManagement">戻る</a></div>
	            </form>
            </div>
            <div class="copyright">Copyright(c)takamatsu</div>
        </div>
    </body>
</html>