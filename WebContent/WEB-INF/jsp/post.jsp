<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta charset="UTF-8">
<title>新規投稿</title>
</head>
<body>
	<div class="main-contants">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="inputfrom">
			<form action="newPost" method="post">
				<label for="title">タイトル</label><input type="text" id="title" name="title" value="${newPost.title}" placeholder="30字以内で入力してください"/><br />
				<label for="postText">本文</label><textarea name="postText"id="postText" rows="" cols="" placeholder="1000字以内で入力してください">${newPost.text}</textarea><br />
				<label for="category">カテゴリー</label><input type="text" id="category" name="category" value="${newPost.category}" placeholder="10字以内で入力してください"/><br />
				<br /><div class="submits"><button type="submit">投稿</button>　　　　　<a href="home">戻る</a></div>
			</form>
		</div>
	</div>
</body>
</html>