<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ホーム</title>
</head>
<body>
<script>
	function postDeleteFunction(id) {
		if(confirm("この投稿を削除してもいいですか？")){
			var url = "postDelete?postDelete=" + id;
			window.location.href = url;
		} else {
			alert( "削除をキャンセルしました。") ;
		}
	}

	function commentDeleteFunction(id) {
		if(confirm("このコメントを削除してもいいですか？")){
			var url = "commentDelete?commentDelete=" + id;
			window.location.href = url;
		} else {
			alert( "削除をキャンセルしました。") ;
		}
	}

	function commentListDisp(id) {
		obj=document.getElementById('commentList' + id).style;
		obj.display=(obj.display=='none')?'block':'none';
	}
</script>
	<div class="main-contants">
		<a href="UserManagement">ユーザー管理</a> <a href="newPost">新規投稿</a> <a href="logout">ログアウト</a> <div class="loginUser">ログインユーザー：${loginUser.name}さん</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="posts">
			<div class="searchform">
				<form action="search" method="post">
					<div class="categorySerch">カテゴリー検索：<input type="text" name="searchWord" id="searchWord" placeholder="検索ワードを入れてください"/><br /></div>
					日付検索：
					<input type="text" name="beforeYear" id="beforeYear" placeholder="年度" value="${dateFormat.beforeYear}"/>年
					<input type="text" name="beforeMouth" id="beforeMouth" placeholder="月" value="${dateFormat.beforeMouth}"/>月
					<input type="text" name="beforeDay" id="beforeDay" placeholder="日" value="${dateFormat.beforeDay}"/>日
					～
					<input type="text" name="afterYear" id="afterYear" placeholder="年度" value="${dateFormat.afterYear}"/>年
					<input type="text" name="afterMouth" id="afterMouth" placeholder="月" value="${dateFormat.afterMouth}"/>月
					<input type="text" name="afterDay" id="afterDay" placeholder="日" value="${dateFormat.afterDay}"/>日
					<button type="submit">検索</button>
				</form>
			</div>
			<c:forEach items="${postList}" var="post">
				<c:if test="${!post.delete}" >
					<div class= "post">
						title: ${post.title}<br />
						${post.text}<br />
						<div class="writer">
							投稿者：${post.writterName} 投稿日：${post.createdDate} カテゴリー：${post.category}<br />
							<c:if test="${post.writterId.equals(loginUser.id)}">
								<button onclick="postDeleteFunction('${post.id}')" >削除</button>
							</c:if>
						</div>
					</div>

					<div class="comment">
						<div onclick=commentListDisp(${post.id})>
							<a style="cursor:pointer;">▶コメント一覧</a>
						</div>
						<div id="commentList${post.id}" style="display:none; clear:both;">
							<c:forEach items="${post.comments}" var="comment">
								<c:if test="${!comment.delete}" >
									<h4>${comment.text}</h4><br />
									<div class="writer">${comment.writterName} : ${comment.createdDate}<br />
										<c:if test="${comment.writterId.equals(loginUser.id)}">
											<button onclick="commentDeleteFunction('${comment.id}')" >削除</button>
										</c:if>
									</div>
								</c:if>
							</c:forEach>
							<form action="commentpost" method="post">
								<textarea name="comment" id="comment" rows="" cols="" placeholder="500字以内で入力して下さい。" ></textarea>
								<input type="hidden" name="postId" id="postId" value="${post.id}" />
								<button type="submit">コメント</button>
							</form>
						</div>
					</div>
				</c:if>
			</c:forEach>
		</div>
	</div>
</body>
</html>