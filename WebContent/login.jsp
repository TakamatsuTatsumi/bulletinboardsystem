<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ログイン画面</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="inputfrom">
			<form action="index.jsp" method="post">
				<br /> <label for="loginId">ログインID</label> <input name="loginId"
					id="loginId" value="${loginId}" /><br /> <label for="password">パスワード</label>
				<input name="password" type="password" id="password" /> <br />
				<button type="submit">ログイン</button><br />
			</form>
		</div>
		<div class="copyright">Copyright(c)takamatsu</div>
	</div>
</body>
</html>