package takamatsu_tatsumi.dao;

import static takamatsu_tatsumi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.exception.SQLRuntimeException;

public class UserDao {
	public boolean isUniqueUser(Connection connection, User user) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select * from users where login_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, user.getLoginId());

			rs = ps.executeQuery();
			return rs.next();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void insert(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			String sql = "insert into users (login_id, password, name, branch_code, department_code, is_stopped, created_date )"
					+ " values (?, ?, ?, (select id from branches where name = ?), (select id from departments where name = ?), ?, CURRENT_TIMESTAMP)";
			ps = connection.prepareStatement(sql);

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setString(4, user.getBranchCode());
			ps.setString(5, user.getDepartmentCode());
			ps.setBoolean(6, user.isStopped());

			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			String sql = "update users set"
					+ " login_id = ?, password = (case ? is null when 1 then password else ? end), name = ?,"
					+ " branch_code = (select id from branches where name =  ?),"
					+ " department_code = (select id from departments where name = ?), is_stopped = ?, created_date = created_date"
					+ " where id = ?";
			ps = connection.prepareStatement(sql);

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getName());
			ps.setString(5, user.getBranchCode());
			ps.setString(6, user.getDepartmentCode());
			ps.setBoolean(7, user.isStopped());
			ps.setInt(8, user.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String loginId, String password) {

		PreparedStatement ps = null;
		PreparedStatement loginDateInsert = null;
		try {
			String sql = "select users.id, users.name, password, login_id, branches.name, departments.name,"
					+ " is_stopped+0, users.created_date"
					+ " from users"
					+ " inner join branches on users.branch_code = branches.id"
					+ " inner join departments on users.department_code = departments.id"
					+ " where login_id = ? and password = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			}else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			}else {
				String loginDateSql = "update users set login_date = CURRENT_TIMESTAMP where login_id = ?";
				loginDateInsert = connection.prepareStatement(loginDateSql);
				loginDateInsert.setString(1, loginId);
				loginDateInsert.executeUpdate();
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(loginDateInsert);
			close(ps);
		}
	}

	public void switchStopped(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			String sql = "update users set is_stopped = ?, created_date = created_date where id = ?";
			ps = connection.prepareStatement(sql);

			ps.setInt(1, user.isStopped() ? 0 : 1);
			ps.setInt(2, user.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getEditUser(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			String sql = "select users.id, users.name, password, login_id, branches.name, departments.name,"
					+ " is_stopped+0, users.created_date"
					+ " from users "
					+ " inner join branches on users.branch_code = branches.id"
					+ " inner join departments on users.department_code = departments.id"
					+ " where users.id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, user.getId());

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> getAllUser(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			String sql = "select users.id, users.name, password, login_id, branches.name, departments.name,"
					+ " is_stopped+0, users.created_date"
					+ " from users"
					+ " inner join branches on users.branch_code = branches.id"
					+ " inner join departments on users.department_code = departments.id"
					+ " where users.id != ?"
					+ " order by id asc";
			ps = connection.prepareStatement(sql);

			ps.setInt(1, user.getId());

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			return userList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("users.id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("users.name");
				String password = rs.getString("password");
				String branchCode = rs.getString("branches.name");
				String departmentCode = rs.getString("departments.name");
				boolean isStopped = (rs.getInt("is_stopped+0") == 1);
				Timestamp createdDate = rs.getTimestamp("users.created_date");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setName(name);
				user.setPassword(password);
				user.setBranchCode(branchCode);
				user.setDepartmentCode(departmentCode);
				user.setStopped(isStopped);
				user.setCreatedDate(createdDate);

				ret.add(user);
			}
			return ret;
	    } finally {
	        close(rs);
	    }
	}
}
