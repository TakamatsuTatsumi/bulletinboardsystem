package takamatsu_tatsumi.dao;

import static takamatsu_tatsumi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import takamatsu_tatsumi.beans.Comment;
import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.exception.SQLRuntimeException;

public class CommentDao {
	public void insert(Connection connection, Comment comment) {
		PreparedStatement ps = null;
		try {
			String sql = "insert into comments ( text, post_id, writter_id, is_delete )"
					+ " values (?, ?, (select id from users where name = ?), ?)";
			ps = connection.prepareStatement(sql);

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getPostId());
			ps.setString(3, comment.getWritterName());
			ps.setBoolean(4, comment.isDelete());

			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Comment> getCommentList(Connection connection, Post post) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select comments.id, comments.text, comments.post_id, users.id, users.name, comments.created_date, comments.is_delete+0"
					+ " from comments"
					+ " inner join users on users.id = comments.writter_id"
					+ " where comments.post_id = ?";
			ps = connection.prepareStatement(sql);

			ps.setInt(1, post.getId());

			rs = ps.executeQuery();
			List<Comment> commentList = toCommentList(rs);
			return commentList;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(rs);
			close(ps);
		}
	}

	private List<Comment> toCommentList(ResultSet rs) throws SQLException {
		List<Comment> commentList = new ArrayList<>();


		while(rs.next()) {
			Comment comment = new Comment();
			int id = rs.getInt("comments.id");
			String text = rs.getString("comments.text");
			int postId = rs.getInt("comments.post_id");
			int userId = rs.getInt("users.id");
			String writterName = rs.getString("users.name");
			boolean isDelete = (rs.getInt("comments.is_delete+0") == 1);
			Date createdDate = rs.getDate("comments.created_date");

			comment.setId(id);
			comment.setText(text);
			comment.setPostId(postId);
			comment.setWritterId(userId);
			comment.setWritterName(writterName);
			comment.setDelete(isDelete);
			comment.setCreatedDate(createdDate);

			commentList.add(comment);
		}

		return commentList;
	}

	public void delete(Connection connection, Comment comment) {
		PreparedStatement ps = null;
		try {
			String sql = "update comments set is_delete = 1, created_date = created_date where id = ?;";
			ps = connection.prepareStatement(sql);

			ps.setInt(1, comment.getId());

			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
