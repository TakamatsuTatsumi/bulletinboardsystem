package takamatsu_tatsumi.dao;

import static takamatsu_tatsumi.utils.CloseableUtil.*;
import static takamatsu_tatsumi.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import takamatsu_tatsumi.beans.Comment;
import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.beans.Search;
import takamatsu_tatsumi.exception.SQLRuntimeException;
import takamatsu_tatsumi.service.CommentService;

public class PostDao {

	public void insert(Connection connection, Post post) {
		PreparedStatement ps = null;
		try {
			String sql = "insert into posts ( title, text, category, writter_id, is_delete ) values (?, ?, ?, ?, 0);";
			ps = connection.prepareStatement(sql);

			ps.setString(1, post.getTitle());
			ps.setString(2, post.getText());
			ps.setString(3, post.getCategory());
			ps.setInt(4, post.getWritterId());

			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, Post post) {
		PreparedStatement ps = null;
		try {
			String sql = "update posts, comments set posts.is_delete = 1, comments.is_delete = 1,"
					+ " posts.created_date = posts.created_date, comments.created_date = comments.created_date"
					+ " where (posts.id = ? and comments.post_id = ?) or posts.id = ?;";
			ps = connection.prepareStatement(sql);

			ps.setInt(1, post.getId());
			ps.setInt(2, post.getId());
			ps.setInt(3, post.getId());

			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Post> getPostList(Connection connection, String searchWord) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Post> postList = null;
		searchWord = SQL_WILDCARD + searchWord + SQL_WILDCARD;
		try {
			String sql = "select posts.id, posts.title, posts.text, posts.category, users.id,"
					+ " users.name, posts.is_delete+0, posts.created_date"
					+ " from posts"
					+ " inner join users on users.id = posts.writter_id"
					+ " where category like ?"
					+ " order by posts.id desc";
			ps = connection.prepareStatement(sql);

			ps.setString(1, searchWord);

			rs = ps.executeQuery();
			postList = toPostList(rs);

		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			close(rs);
			close(ps);
		}
		return postList;
	}

	public List<Post> getSearchDatePostList(Connection connection, Search search){
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Post> postList = null;
		try {
			String sql = "select posts.id, posts.title, posts.text, posts.category, users.id,"
					+ " users.name, posts.is_delete+0, posts.created_date"
					+ " from posts"
					+ " inner join users on users.id = posts.writter_id"
					+ " where posts.created_date between ? and ?";
			ps = connection.prepareStatement(sql);

			ps.setDate(1, search.getSearchBeforeDate());
			ps.setDate(2, search.getSearchAfterDate());

			rs = ps.executeQuery();
			postList = toPostList(rs);

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(rs);
			close(ps);
		}
		return postList;

	}

	public List<Post> getCategoryAndDateSearchList(Connection connection, Search search) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Post> postList = null;
		search.setSearchWord(SQL_WILDCARD + search.getSearchWord() + SQL_WILDCARD);
		try {
			String sql = "select posts.id, posts.title, posts.text, posts.category, users.id,"
					+ " users.name, posts.is_delete+0, posts.created_date"
					+ " from posts"
					+ " inner join users on users.id = posts.writter_id"
					+ " where posts.created_date between ? and ?"
					+ " and category like ?";

			ps = connection.prepareStatement(sql);

			ps.setDate(1, search.getSearchBeforeDate());
			ps.setDate(2, search.getSearchAfterDate());
			ps.setString(3, search.getSearchWord());

			rs = ps.executeQuery();
			postList = toPostList(rs);

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(rs);
			close(ps);
		}
		return postList;

	}

	private List<Post> toPostList(ResultSet rs) throws SQLException {
		List<Post> postList = new ArrayList<>();

		List<Comment> commentLsit = new ArrayList<>();

		while(rs.next()) {
			Post post = new Post();
			int id = rs.getInt("posts.id");
			String title = rs.getString("posts.title");
			String text = rs.getString("posts.text");
			String category = rs.getString("posts.category");
			int writterId = rs.getInt("users.id");
			String writterName = rs.getString("users.name");
			boolean isDelete = (rs.getInt("posts.is_delete+0") == 1);
			Date createdDate = rs.getDate("posts.created_date");

			post.setId(id);
			post.setTitle(title);
			post.setText(text);
			post.setCategory(category);
			post.setWritterId(writterId);
			post.setWritterName(writterName);
			post.setDelete(isDelete);
			post.setCreatedDate(createdDate);

			commentLsit = new CommentService().getCommentList(post);
			post.setComments(commentLsit);

			postList.add(post);
		}

		return postList;
	}

}
