package takamatsu_tatsumi.utils;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import takamatsu_tatsumi.beans.Comment;
import takamatsu_tatsumi.beans.DateFormat;
import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.beans.User;

public class CheckUtil {
	private static final String MOUTH_REGX = "([1-9]|1[0-2])";

	private static final String DAY_REGX = "([1-9]|[12][0-9]|3[01])";

	public static List<String> isValid(List<String> messages, User user, boolean isSignup) {
		if(StringUtils.isEmpty(user.getLoginId()) == true) {
			messages.add("ログインIDを入力してください");
		}
		if( !user.getLoginId().matches("[a-zA-Z0-9]+") ) {
			messages.add("ログインIDに不正な文字があります。");
		}
		if(user.getLoginId().length() < 6 || user.getLoginId().length() > 20) {
			messages.add("ログインIDの文字数が不正です。6字以上20字以内で入力してください");
		}

		if(isSignup) {
			if(StringUtils.isEmpty(user.getPassword())) {
				messages.add("パスワードを入力してください");
			}
			if(!user.getPassword().equals(user.getRepassword())) {
				messages.add("パスワードと確認パスワードが一致していません");
			}
			if(!user.getPassword().matches("[ -~]+")) {
				messages.add("パスワードに不正な文字があります。");
			}
			if(user.getPassword().length() < 6 || user.getPassword().length() > 20) {
				messages.add("パスワードの文字数が不正です。6字以上20字以内で入力してください");
			}
		} else {
			if(!StringUtils.isEmpty(user.getPassword())){
				if(!user.getPassword().equals(user.getRepassword())) {
					messages.add("パスワードと確認パスワードが一致していません");
				}
				if(!user.getPassword().matches("[ -~]+")) {
					messages.add("パスワードに不正な文字があります");
				}
				if(user.getPassword().length() < 6 || user.getPassword().length() > 20) {
					messages.add("パスワードの文字数が不正です。6字以上20字以内で入力してください");
				}
			}
		}

		if(StringUtils.isEmpty(user.getName()) == true) {
			messages.add("名前を入力してください");
		}
		if(user.getName().length() > 10) {
			messages.add("名前の文字数が10文字以上です。10文字以下で入力してください。");
		}
		if(StringUtils.isEmpty(user.getBranchCode()) == true) {
			messages.add("支店名を入力してください");
		}
		if(StringUtils.isEmpty(user.getDepartmentCode()) == true) {
			messages.add("部署・役職を入力してください");
		} else {
			if(user.getDepartmentCode().equals("") || (user.getDepartmentCode().equals("総務人事担当者") && !user.getBranchCode().equals("本社")) || (user.getDepartmentCode().equals("情報管理担当者") && !user.getBranchCode().equals("本社"))) {
				messages.add("本社以外の支店で本社のみの役職で登録しようとしています");
			}
		}
		return messages;
	}

	public static List<String> isOKPost(List<String> messages, Post post) {
		if(StringUtils.isEmpty(post.getTitle())) {
			messages.add("タイトルが未入力です");
		} else {
			if(post.getTitle().length() > 30) {
				messages.add("タイトルが30文字を超えています");
			}
		}

		if(StringUtils.isEmpty(post.getText())) {
			messages.add("本文が未入力です");
		} else {
			if(post.getText().length() > 1000) {
				messages.add("本文が1000文字を超えています");
			}
		}

		if(StringUtils.isEmpty(post.getCategory())) {
			messages.add("カテゴリーが未入力です");
		} else {
			if(post.getCategory().length() > 10) {
				messages.add("カテゴリーが10文字を超えています");
			}
		}
		return messages;
	}

	public static List<String> isOKComment(List<String> messages, Comment comment) {
		if(StringUtils.isEmpty(comment.getText())) {
			messages.add("コメントを入力してください");
		} else {
			if(comment.getText().length() > 500) {
				messages.add("コメントが500文字を超えています");
			}
		}

		return messages;
	}

	public static List<String> isOKDateFormat(List<String> messages, DateFormat dateFormat){
		if(StringUtils.isEmpty(dateFormat.getBeforeYear()) || StringUtils.isEmpty(dateFormat.getBeforeMouth())
				|| StringUtils.isEmpty(dateFormat.getBeforeDay())) {

			if(!StringUtils.isEmpty(dateFormat.getAfterYear()) || !StringUtils.isEmpty(dateFormat.getAfterMouth())
					|| !StringUtils.isEmpty(dateFormat.getAfterDay())) {
				messages.add("過去の日付が指定されていません");
			} else {
				dateFormat.setNotDateSearch(true);
			}
		}

		if((!StringUtils.isEmpty(dateFormat.getBeforeYear()) && !dateFormat.getBeforeYear().matches("\\d{4}")) ||
				(!StringUtils.isEmpty(dateFormat.getAfterYear()) && !dateFormat.getAfterYear().matches("\\d{4}"))) {
			messages.add("年に不正な値が入力されています。");
		}
		if((!StringUtils.isEmpty(dateFormat.getBeforeMouth()) && !dateFormat.getBeforeMouth().matches(MOUTH_REGX)) ||
				(!StringUtils.isEmpty(dateFormat.getAfterMouth()) && !dateFormat.getAfterMouth().matches(MOUTH_REGX))) {
			messages.add("月に不正な値が入力されています。");
		}
		if((!StringUtils.isEmpty(dateFormat.getBeforeDay()) && !dateFormat.getBeforeDay().matches(DAY_REGX)) ||
				(!StringUtils.isEmpty(dateFormat.getAfterDay()) && !dateFormat.getBeforeDay().matches(DAY_REGX))) {
			messages.add("日に不正な値が入力されています。");
		}


		return messages;

	}
}
