package takamatsu_tatsumi.service;

import static takamatsu_tatsumi.utils.CloseableUtil.*;
import static takamatsu_tatsumi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.dao.UserDao;
import takamatsu_tatsumi.utils.CipherUtil;

public class UserService {

	public void register(User user, List<String> messages) {
		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);
            UserDao userDao = new UserDao();
            if(!userDao.isUniqueUser(connection, user) || messages.isEmpty()) {
            	new UserDao().insert(connection, user);
            } else {
            	messages.add("すでに存在するログインIDです");
            }

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public List<User> getUserList(User user) {
		Connection connection = null;
		try {
			connection = getConnection();

			List<User> userList = new UserDao().getAllUser(connection, user);
			commit(connection);
			return userList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = null;
            String passWord = user.getPassword();
            if(passWord != null && !passWord.equals("")) {
            	encPassword = CipherUtil.encrypt(passWord);
            }
            user.setPassword(encPassword);

            new UserDao().update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public User getEditUser(User user) {
		Connection connection = null;
		try {
			connection = getConnection();

			User editUser = new UserDao().getEditUser(connection, user);
			commit(connection);
			return editUser;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void isStopped(User user) {
		Connection connection = null;
		try {
			connection = getConnection();

			new UserDao().switchStopped(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
