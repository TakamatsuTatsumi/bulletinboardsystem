package takamatsu_tatsumi.service;

import static takamatsu_tatsumi.utils.CloseableUtil.*;
import static takamatsu_tatsumi.utils.DBUtil.*;

import java.sql.Connection;

import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.dao.UserDao;
import takamatsu_tatsumi.exception.SQLRuntimeException;
import takamatsu_tatsumi.utils.CipherUtil;

public class LoginService {
	public User login(String loginId, String password) {
		Connection connection = null;

		try {
			connection = getConnection();

			User user = new User();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			user = userDao.getUser(connection, loginId, encPassword);

			commit(connection);
			return user;
		} catch (SQLRuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
