package takamatsu_tatsumi.service;

import static takamatsu_tatsumi.utils.CloseableUtil.*;
import static takamatsu_tatsumi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.dao.PostDao;
import takamatsu_tatsumi.exception.SQLRuntimeException;

public class PostService {
	public List<Post> getPostList() {
		Connection connection = null;
		try {
			connection = getConnection();

			List<Post> postList = new PostDao().getPostList(connection, SQL_WILDCARD);
			commit(connection);
			return postList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void insert(Post post) {
		Connection connection = null;
		try {
			connection = getConnection();

			new PostDao().insert(connection, post);
			commit(connection);
		} catch (SQLRuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(Post post) {
		Connection connection = null;
		try {
			connection = getConnection();

			new PostDao().delete(connection, post);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
