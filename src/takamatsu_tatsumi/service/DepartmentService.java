package takamatsu_tatsumi.service;

import static takamatsu_tatsumi.utils.CloseableUtil.*;
import static takamatsu_tatsumi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import takamatsu_tatsumi.beans.Department;
import takamatsu_tatsumi.dao.DepartmentDao;

public class DepartmentService {
	public List<Department> getDepartmentList() {
		Connection connection = null;
		try {
			connection = getConnection();

			List<Department> departmentList = new DepartmentDao().getDepartments(connection);
			commit(connection);
			return departmentList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
