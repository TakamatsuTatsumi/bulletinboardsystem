package takamatsu_tatsumi.service;

import static takamatsu_tatsumi.utils.CloseableUtil.*;
import static takamatsu_tatsumi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.beans.Search;
import takamatsu_tatsumi.dao.PostDao;

public class SearchService {

	public List<Post> categorySearch(Search search) {
		Connection connection = null;
		try {

			connection = getConnection();

			List<Post> postList = new PostDao().getPostList(connection, search.getSearchWord());

			return postList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Post> dateSearch(Search search) {
		Connection connection = null;
		try {
			connection = getConnection();

			List<Post> postList = new PostDao().getSearchDatePostList(connection, search);

			return postList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Post> categoryAndDateSearch(Search search) {
		Connection connection = null;
		try {
			connection = getConnection();

			List<Post> postList = new PostDao().getCategoryAndDateSearchList(connection, search);

			return postList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
