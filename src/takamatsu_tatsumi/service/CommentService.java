package takamatsu_tatsumi.service;

import static takamatsu_tatsumi.utils.CloseableUtil.*;
import static takamatsu_tatsumi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import takamatsu_tatsumi.beans.Comment;
import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.dao.CommentDao;

public class CommentService {
	public List<Comment> getCommentList(Post post) {
		Connection connection = null;
		try {
			connection = getConnection();

			List<Comment> commentList = new CommentDao().getCommentList(connection, post);
			commit(connection);
			return commentList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void registar(Comment comment) {
		Connection connection = null;
		try {
			connection = getConnection();

			new CommentDao().insert(connection, comment);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(Comment comment) {
		Connection connection = null;
		try {
			connection = getConnection();

			new CommentDao().delete(connection, comment);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
