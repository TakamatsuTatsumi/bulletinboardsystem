package takamatsu_tatsumi.service;

import static takamatsu_tatsumi.utils.CloseableUtil.*;
import static takamatsu_tatsumi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import takamatsu_tatsumi.beans.Branch;
import takamatsu_tatsumi.dao.BranchDao;

public class BranchService {
	public List<Branch> getBranchList() {
		Connection connection = null;
		try {
			connection = getConnection();

			List<Branch> branchList = new BranchDao().getBranchs(connection);
			commit(connection);
			return branchList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
