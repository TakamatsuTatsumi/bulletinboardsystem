package takamatsu_tatsumi.beans;

import java.io.Serializable;

public class DateFormat implements Serializable {
	private String beforeYear;
	private String beforeMouth;
	private String beforeDay;
	private String afterYear;
	private String afterMouth;
	private String afterDay;
	private boolean isNotDateSearch;

	public DateFormat() {

	}

	public String getBeforeYear() {
		return beforeYear;
	}

	public void setBeforeYear(String beforeYear) {
		this.beforeYear = beforeYear;
	}

	public String getBeforeMouth() {
		return beforeMouth;
	}

	public void setBeforeMouth(String beforeMouth) {
		this.beforeMouth = beforeMouth;
	}

	public String getBeforeDay() {
		return beforeDay;
	}

	public void setBeforeDay(String beforeDay) {
		this.beforeDay = beforeDay;
	}

	public String getAfterYear() {
		return afterYear;
	}

	public void setAfterYear(String afterYear) {
		this.afterYear = afterYear;
	}

	public String getAfterMouth() {
		return afterMouth;
	}

	public void setAfterMouth(String afterMouth) {
		this.afterMouth = afterMouth;
	}

	public String getAfterDay() {
		return afterDay;
	}

	public void setAfterDay(String afterDay) {
		this.afterDay = afterDay;
	}

	public boolean isNotDateSearch() {
		return isNotDateSearch;
	}

	public void setNotDateSearch(boolean isNotDateSearch) {
		this.isNotDateSearch = isNotDateSearch;
	}
}
