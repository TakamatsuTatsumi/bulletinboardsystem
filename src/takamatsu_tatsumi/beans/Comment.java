package takamatsu_tatsumi.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
	private int id;
	private String text;
	private int postId;
	private int writterId;
	private String writterName;
	private boolean isDelete;
	private Date createdDate;

	public Comment() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public int getWritterId() {
		return writterId;
	}

	public void setWritterId(int writterId) {
		this.writterId = writterId;
	}

	public String getWritterName() {
		return writterName;
	}

	public void setWritterName(String writterName) {
		this.writterName = writterName;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", text=" + text + ", postId=" + postId + ", writterName=" + writterName
				+ ", createdDate=" + createdDate + "]";
	}
}
