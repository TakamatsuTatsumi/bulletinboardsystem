package takamatsu_tatsumi.beans;

import java.io.Serializable;

public class Branch implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String name;

	public Branch() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
