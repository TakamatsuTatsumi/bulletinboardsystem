package takamatsu_tatsumi.beans;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class Search implements Serializable {
	private String searchWord;
	private String searchDateString;
	private Date searchBeforeDate;
	private Date searchAfterDate;
	private List<Post> postList;


	public Search() {

	}

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public String getSearchDateString() {
		return searchDateString;
	}

	public void setSearchDateString(String searchDateString) {
		this.searchDateString = searchDateString;
	}

	public Date getSearchBeforeDate() {
		return searchBeforeDate;
	}

	public void setSearchBeforeDate(Date searchBeforeDate) {
		this.searchBeforeDate = searchBeforeDate;
	}

	public Date getSearchAfterDate() {
		return searchAfterDate;
	}

	public void setSearchAfterDate(Date searchAfterDate) {
		this.searchAfterDate = searchAfterDate;
	}

	public List<Post> getPostList() {
		return postList;
	}

	public void setPostList(List<Post> postList) {
		this.postList = postList;
	}

}
