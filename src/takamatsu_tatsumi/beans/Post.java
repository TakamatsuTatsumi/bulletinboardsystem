package takamatsu_tatsumi.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Post implements Serializable {
	private int id;
	private String title;
	private String text;
	private String category;
	private String writterName;
	private int writterId;
	private boolean isDelete;
	private Date createdDate;
	private List<Comment> comments;

	public Post() {

	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getWritterName() {
		return writterName;
	}
	public void setWritterName(String writterName) {
		this.writterName = writterName;
	}
	public int getWritterId() {
		return writterId;
	}
	public void setWritterId(int writterId) {
		this.writterId = writterId;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
}
