package takamatsu_tatsumi.test;

import java.util.ArrayList;
import java.util.List;

import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.utils.CheckUtil;


public class CheckUtilsTest {

	public static void main(String[] args) {

		Post postNG1 = new Post();
		Post postNG2 = new Post();
		Post postNG3 = new Post();
		Post postNG4 = new Post();

		Post postOK1 = new Post();

		String str31 = "abcdefghijklmnopqrstuvwxyzabcde";
		String str11 = "abcdefghijk";

		StringBuilder sb = new StringBuilder();
		for(int i = 0; i <= 1000; i++) {
			sb.append("a");
		}

		postNG2.setCategory(str11);
		postNG2.setTitle(str31);
		System.out.println(sb.toString());
		postNG2.setText(sb.toString());

		String strNG = "うんち";
		String strNG2 = "NGワード";

		postNG3.setCategory(strNG);
		postNG3.setText(strNG2);
		postNG3.setTitle(strNG);

		String strNG3 = "****うんち";
		String strNG4 = "**NGワード*";

		postNG4.setCategory(strNG3);
		postNG4.setText(strNG4);
		postNG4.setTitle(strNG3);

		String strOK = "testtest";

		postOK1.setCategory(strOK);
		postOK1.setText(strOK);
		postOK1.setTitle(strOK);

		List<String> messages = new ArrayList<>();

		CheckUtil.isOKPost(messages, postNG1);
		disp(messages);

		CheckUtil.isOKPost(messages, postNG2);
		disp(messages);

		CheckUtil.isOKPost(messages, postNG3);
		disp(messages);

		CheckUtil.isOKPost(messages, postNG4);
		disp(messages);

		CheckUtil.isOKPost(messages, postOK1);
		disp(messages);
	}

	private static void disp(List<String> messages) {
		if(messages.isEmpty()) {
			System.out.println("エラーなし");
		}
		for(String str : messages) {
			System.out.println(str);
		}
		System.out.println();
		messages.removeAll(messages);
	}
}
