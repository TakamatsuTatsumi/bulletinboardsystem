package takamatsu_tatsumi.test;

import java.util.ArrayList;
import java.util.List;

import takamatsu_tatsumi.beans.Comment;
import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.service.PostService;

public class PostTest {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		//Post post = new Post();
		List<Post> pList = new ArrayList<>();
		PostService ps = new PostService();

		pList = ps.getPostList();

		for(Post post : pList) {
			List<Comment> comes = post.getComments();
			for(Comment come : comes) {
				System.out.println(come.getText() + "  " + come.getId());
			}
		}
	}

}
