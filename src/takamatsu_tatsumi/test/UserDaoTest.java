package takamatsu_tatsumi.test;

import java.util.ArrayList;
import java.util.List;

import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.UserService;

public class UserDaoTest {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		User user = new User();

		List<String> messages = new ArrayList<>();
		user.setLoginId("hoge2");
		user.setPassword("hoge");
		user.setName("hoge2");
		user.setBranchCode("本社");
		user.setDepartmentCode("社員1");
		new UserService().register(user, messages);

		for(String m : messages) {
			System.out.println(m);
		}
	}

}
