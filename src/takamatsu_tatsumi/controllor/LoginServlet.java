package takamatsu_tatsumi.controllor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.LoginService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(StringUtils.isEmpty(loginId) || StringUtils.isEmpty(password)) {
			messages.add("ログインIDまたはパスワードが入力されていません");
		} else {
			LoginService loginService = new LoginService();
			User user = loginService.login(loginId, password);

			if (user != null && !user.isStopped()) {
				session.setAttribute("loginUser", user);
				request.getRequestDispatcher("home").forward(request, response);
				return;
			}
			if(user == null || (user != null && user.isStopped())) {
				messages.add("ログインIDまたはパスワードが誤っています");
			}

		}
		session.setAttribute("errorMessages", messages);
		session.setAttribute("loginId", loginId);
		doGet(request, response);
	}

}
