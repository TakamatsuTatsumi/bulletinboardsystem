package takamatsu_tatsumi.controllor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.UserService;

@WebServlet(urlPatterns = {"/stopped"})
public class StoppedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int editUserId = Integer.parseInt(request.getParameter("stoppedId"));
		User user = new User();
		user.setId(editUserId);
		UserService userService = new UserService();
		user = userService.getEditUser(user);
		userService.isStopped(user);
		request.getRequestDispatcher("UserManagement").forward(request, response);
	}

}
