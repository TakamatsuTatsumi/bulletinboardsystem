package takamatsu_tatsumi.controllor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.UserService;

@WebServlet("/UserManagement")
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		if(user != null && user.getDepartmentCode().equals("総務人事担当者")) {
			List<User> userList = new UserService().getUserList(user);
			request.setAttribute("userList", userList);
			request.getRequestDispatcher("/WEB-INF/jsp/UserManagement.jsp").forward(request, response);
		} else {
			List<String> messages = new ArrayList<>();
			messages.add("権限を持っていないためユーザー管理を利用することができません");
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("home").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
