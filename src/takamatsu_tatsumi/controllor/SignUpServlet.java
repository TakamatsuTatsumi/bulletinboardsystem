package takamatsu_tatsumi.controllor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takamatsu_tatsumi.beans.Branch;
import takamatsu_tatsumi.beans.Department;
import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.BranchService;
import takamatsu_tatsumi.service.DepartmentService;
import takamatsu_tatsumi.service.UserService;
import takamatsu_tatsumi.utils.CheckUtil;


@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Branch> branchList = new BranchService().getBranchList();
		List<Department> departmentList = new DepartmentService().getDepartmentList();

		HttpSession session = request.getSession();

		session.setAttribute("branchList", branchList);
		session.setAttribute("departmentList", departmentList);

		request.getRequestDispatcher("/WEB-INF/jsp/signup.jsp").forward(request, response);
		return;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		if(loginUser != null && loginUser.getDepartmentCode().equals("総務人事担当者")) {
			User newUser = getUser(request);

			List<String> messages = new ArrayList<String>();
			messages = CheckUtil.isValid(messages, newUser, true);

			if(!messages.isEmpty()) {
				session.setAttribute("errorMessages", messages);
				session.setAttribute("newUser", newUser);
				response.sendRedirect("signup");
				return;
			}

			new UserService().register(newUser, messages);
			session.removeAttribute("newUser");
			request.getRequestDispatcher("UserManagement").forward(request, response);
			return;

		} else {
			List<String> messages = new ArrayList<>();
			messages.add("権限を持っていないためユーザー管理を利用することができません");
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("home").forward(request, response);
			return;
		}
	}

	private User getUser (HttpServletRequest request) {
		User newUser = new User();
		newUser.setLoginId(request.getParameter("loginId"));
		newUser.setPassword(request.getParameter("password"));
		newUser.setRepassword(request.getParameter("repassword"));
		newUser.setName(request.getParameter("name"));
		newUser.setBranchCode(request.getParameter("branchCode"));
		newUser.setDepartmentCode(request.getParameter("departmentCode"));
		newUser.setStopped(false);
		return newUser;
	}
}
