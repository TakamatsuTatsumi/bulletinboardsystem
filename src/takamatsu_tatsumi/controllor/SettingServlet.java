package takamatsu_tatsumi.controllor;
import static takamatsu_tatsumi.utils.CheckUtil.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takamatsu_tatsumi.beans.Branch;
import takamatsu_tatsumi.beans.Department;
import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.BranchService;
import takamatsu_tatsumi.service.DepartmentService;
import takamatsu_tatsumi.service.UserService;;

@WebServlet(urlPatterns = {"/settings"})
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		User loginUser = (User) session.getAttribute("loginUser");

		if(loginUser != null && loginUser.getDepartmentCode().equals("総務人事担当者")) {
			List<Branch> branchList = new BranchService().getBranchList();
			List<Department> departmentList = new DepartmentService().getDepartmentList();

			String userIdString = request.getParameter("id");
			User editUser = new User();
			if(userIdString != null) {
				int userId = Integer.parseInt(userIdString);
				editUser.setId(userId);
				editUser = new UserService().getEditUser(editUser);
			} else {
				editUser = (User) session.getAttribute("editUser");
			}

			session.setAttribute("editUser", editUser);
			session.setAttribute("branchList", branchList);
			session.setAttribute("departmentList", departmentList);

			request.getRequestDispatcher("/WEB-INF/jsp/settings.jsp").forward(request, response);
		} else {
			List<String> messages = new ArrayList<>();
			messages.add("権限を持っていないためユーザー管理を利用することができません");
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("home").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		User editUser = getUser(request);

		messages = isValid(messages, editUser, false);
		if(messages.isEmpty()) {
			new UserService().update(editUser);
			session.removeAttribute("editUser");
			request.getRequestDispatcher("UserManagement").forward(request, response);
			return;
		} else {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("editUser", editUser);
            doGet(request, response);
		}
	}

	private User getUser (HttpServletRequest request) {
		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setRepassword(request.getParameter("repassword"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranchCode(request.getParameter("branchCode"));
		editUser.setDepartmentCode(request.getParameter("departmentCode"));
		editUser.setStopped(Boolean.parseBoolean(request.getParameter("stopped")));
		return editUser;
	}
}
