package takamatsu_tatsumi.controllor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.PostService;

@WebServlet(urlPatterns = {"/home"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Post> postList = new PostService().getPostList();
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		if(loginUser != null) {
			request.setAttribute("postList", postList);
			request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);
		} else {
			List<String> errorMessages = new ArrayList<>();
			errorMessages.add("ログインをしてください。");
			session.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

}
