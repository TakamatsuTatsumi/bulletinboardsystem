package takamatsu_tatsumi.controllor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takamatsu_tatsumi.beans.Comment;
import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.CommentService;
import takamatsu_tatsumi.utils.CheckUtil;

@WebServlet(urlPatterns = {"/commentpost"})
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("loginUser");
		int postId = Integer.parseInt(request.getParameter("postId"));
		String text = request.getParameter("comment");
		String writterName = user.getName();

		Comment comment = new Comment();
		comment.setPostId(postId);
		comment.setText(text);
		comment.setWritterName(writterName);

		List<String> messages = new ArrayList<>();
		messages = CheckUtil.isOKComment(messages, comment);
		if(messages.isEmpty()) {
			CommentService commentService = new CommentService();
			commentService.registar(comment);

		} else {
			session.setAttribute("errorMessages", messages);
		}

		request.getRequestDispatcher("home").forward(request, response);
	}

}
