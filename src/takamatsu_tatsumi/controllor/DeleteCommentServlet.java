package takamatsu_tatsumi.controllor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import takamatsu_tatsumi.beans.Comment;
import takamatsu_tatsumi.service.CommentService;

@WebServlet(urlPatterns = {"/commentDelete"})
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Comment comment = new Comment();
		comment.setId(Integer.parseInt(request.getParameter("commentDelete")));

		new CommentService().delete(comment);

		request.getRequestDispatcher("home").forward(request, response);
	}

}
