package takamatsu_tatsumi.controllor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.PostService;
import takamatsu_tatsumi.utils.CheckUtil;

@WebServlet(urlPatterns = {"/newPost"})
public class PostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		if(loginUser != null) {
			request.getRequestDispatcher("/WEB-INF/jsp/post.jsp").forward(request, response);
		} else {
			List<String> messages = new ArrayList<>();
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("login").forward(request, response);
			return;
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		Post newPost = new Post();

		newPost.setTitle(request.getParameter("title"));
		newPost.setText(request.getParameter("postText"));
		newPost.setCategory(request.getParameter("category"));
		newPost.setWritterId(loginUser.getId());

		List<String> messages = new ArrayList<>();
		messages = CheckUtil.isOKPost(messages, newPost);
		if(messages.isEmpty()) {
			PostService postService = new PostService();
			postService.insert(newPost);

			request.getRequestDispatcher("home").forward(request, response);
		} else {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("newPost", newPost);

			doGet(request, response);
		}
	}

}
