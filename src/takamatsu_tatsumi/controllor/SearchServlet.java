package takamatsu_tatsumi.controllor;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import takamatsu_tatsumi.beans.DateFormat;
import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.beans.Search;
import takamatsu_tatsumi.beans.User;
import takamatsu_tatsumi.service.SearchService;
import takamatsu_tatsumi.utils.CheckUtil;

@WebServlet(urlPatterns = {"/search"})
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String DATE_SEPARETOR = "_";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		if(loginUser != null) {
			this.doPost(request, response);
		} else {
			List<String> messages = new ArrayList<>();
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("login").forward(request, response);
			return;
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String searchWord = request.getParameter("searchWord");

		List<String> messages = new ArrayList<>();
		DateFormat dateFormat = isVailt(request, messages);

		HttpSession session = request.getSession();

		if(!messages.isEmpty()) {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("dateFormat", dateFormat);

			request.getRequestDispatcher("home").forward(request, response);
			return;
		}

		String searchDate = building(dateFormat);

		session.removeAttribute("postList");

		if(StringUtils.isEmpty(searchWord) && dateFormat.isNotDateSearch()) {

			request.getRequestDispatcher("home").forward(request, response);
			return;
		}

		List<Post> postList = new ArrayList<>();
		Search search = new Search();
		search.setSearchWord(searchWord);
		SearchService searchService = new SearchService();

		if(StringUtils.isEmpty(searchDate) || dateFormat.isNotDateSearch()) {
			postList = searchService.categorySearch(search);
		} else {
			String[] searchDates;
			searchDates = searchDate.split(DATE_SEPARETOR);
			search.setSearchBeforeDate(Date.valueOf(searchDates[0]));
			search.setSearchAfterDate(Date.valueOf(searchDates[1]));
		}

		if(StringUtils.isEmpty(searchWord)) {
			postList = searchService.dateSearch(search);
		}
		if(!StringUtils.isEmpty(searchWord) && !dateFormat.isNotDateSearch()) {
			postList = searchService.categoryAndDateSearch(search);
		}

		session.setAttribute("postList", postList);
		dateFormat.setNotDateSearch(false);
		session.removeAttribute("dateFormat");
		request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);
	}

	private DateFormat isVailt(HttpServletRequest request, List<String> messages) {
		String beforeYear = request.getParameter("beforeYear");
		String beforeMouth = request.getParameter("beforeMouth");
		String beforeDay = request.getParameter("beforeDay");
		String afterYear = request.getParameter("afterYear");
		String afterMouth = request.getParameter("afterMouth");
		String afterDay = request.getParameter("afterDay");

		DateFormat dateFormat = new DateFormat();

		dateFormat.setBeforeYear(beforeYear);
		dateFormat.setBeforeMouth(beforeMouth);
		dateFormat.setBeforeDay(beforeDay);
		dateFormat.setAfterYear(afterYear);
		dateFormat.setAfterMouth(afterMouth);
		dateFormat.setAfterDay(afterDay);

		messages = CheckUtil.isOKDateFormat(messages, dateFormat);

		return dateFormat;
	}

	private String building(DateFormat dateFormat) {
		String DateString = "";

		DateString += dateFormat.getBeforeYear();
		DateString += "-" + dateFormat.getBeforeMouth();
		DateString += "-" + dateFormat.getBeforeDay();
		DateString += DATE_SEPARETOR + dateFormat.getAfterYear();
		DateString += "-" + dateFormat.getAfterMouth();
		DateString += "-" + dateFormat.getAfterDay();

		return DateString;
	}
}
