package takamatsu_tatsumi.controllor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import takamatsu_tatsumi.beans.Post;
import takamatsu_tatsumi.service.PostService;

@WebServlet(urlPatterns = {"/postDelete"})
public class DeletePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Post post = new Post();
		post.setId(Integer.parseInt(request.getParameter("postDelete")));

		new PostService().delete(post);

		request.getRequestDispatcher("home").forward(request, response);
	}

}
